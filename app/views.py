from django.shortcuts import render, redirect
from django.http import HttpResponse
from app.models import Status
from app.forms import StatusForm

# Create your views here.

def index(request):
    form = StatusForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            inputName = form.data['userName']
            inputStatus = form.data['userStatus']
            request.session['inputName'] = inputName
            request.session['inputStatus'] = inputStatus
            # data_item = form.save(commit=False)         
            return redirect('/confirm')
    else:
        # status = StatusForm()
        data = Status.objects.all()
        response = {'Data':data, 'status': form}
        return render(request, 'index.html', response)
    # return render(request, 'index.html')

def confirm(request):
    # form = StatusForm(request.POST)
    # if form.is_valid():
    #     form.save()
    inputName = request.session['inputName']
    inputStatus = request.session['inputStatus']
    if request.method == 'POST':
        if "cancel" in request.POST:
            return redirect('/')
        else:
            # form = {
            #     'userName': inputName,
            #     'userStatus': inputStatus,
            # }
            Status.objects.create(userName = inputName, userStatus = inputStatus)
            # data_item = form.save(commit=False)
            # data_item.save()
            return redirect('/')
    else:
        # context = {
        #     'userName': userName,
        #     'userStatus': userStatus
        # }
        context = {
            'nameName': inputName,
            'statusStatus': inputStatus,
        }
        return render(request, 'confirm.html', context)
    # return render(request, 'confirm.html')
