from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from app.views import index, confirm
from app.models import Status
import time
import unittest

# Create your tests here.

class StoryUnitTest(TestCase):
    def test_story_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    # def test_confirm_url_exist(self):
    #     response = Client().get('confirm')
    #     self.assertEqual(response.status_code, 200)

    def test_story_using_confirm_func(self):
        found = resolve('/confirm')
        self.assertEqual(found.func, confirm)

    # def test_story_using_confirm_template(self):
    #     response = Client().get('confirm')
    #     self.assertTemplateUsed(response, 'confirm.html')

    def test_story_model_exist(self):
        Status.objects.create(userName = "Pewe", userStatus = "Very nice")
        countcontent = Status.objects.all().count()
        self.assertEqual(countcontent, 1)

    def test_can_handle_post_request(self):
        response = self.client.post('/', data={'userName':'joni', 'userStatus':'bermain'})
        self.assertEqual(response.status_code, 302)

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def checkAppFunctionality(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        self.assertIn('Story 7', selenium.title)

        selenium = self.selenium
        selenium.get('http://localhost:8000/message')

        selenium.implicitly_wait(5)

        name = selenium.find_element_by_id('enter-name')
        message = selenium.find_element_by_id('enter-status')
        name.send_keys('joni')
        message.send_keys('im smart')

        save = selenium.find_element_by_class_name('submit')
        save.click()
        selenium.implicitly_wait(5)

        confirm = selenium.find_element_by_class_name('submit')
        confirm.click()

        self.assertIn('joni', selenium.page_source)
        self.assertIn('im smart', selenium.page_source)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()


# if __name__ == '__main__':
#     unittest.main(warnings='ignore')