from django.db import models

# Create your models here.
class Status(models.Model):
    userName = models.CharField(max_length=30)
    userStatus = models.TextField(max_length=200)

    def __str__(self):
        return self.name
